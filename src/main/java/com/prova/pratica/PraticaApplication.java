package com.prova.pratica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class PraticaApplication {

	@RequestMapping("/")
	public String home(){ return "Erick Brener Moreira"; }

	public static void main(String[] args) {
		SpringApplication.run(PraticaApplication.class, args);
	}

}
